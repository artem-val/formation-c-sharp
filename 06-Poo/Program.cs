﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Poo
{
    class Program
    {
        static void Main(string[] args)
        {
            Voiture v1 = new Voiture();
            v1.marque = "Opel";
            Voiture v2 = new Voiture();
            v2.marque = "Fiat";
            Console.WriteLine(v1.marque);
            Console.WriteLine(v2.marque);
            Console.ReadKey();
        }
    }
}
