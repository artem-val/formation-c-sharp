﻿using System;
using System.Text;

namespace _05_methodes
{

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Multiplication(2.8, 1.5));
            Afficher("Bonjour");

            Console.WriteLine(TestMultiReturn(true, 1, 2));
            Console.WriteLine(TestMultiReturn(false, 1, 2));
            Console.WriteLine(Even(2));
            Console.WriteLine(Even(3));
            int param = 8;
            Console.WriteLine("Entrée param={0}", param);
            TestParamValeur(param);
            Console.WriteLine("Sortie param={0}", param);

            StringBuilder sb = new StringBuilder("Valeur Init");
            TestParamValeurReference(sb);
            Console.WriteLine("Sortie param={0}", sb);

            param = 8;
            Console.WriteLine("Entrée param={0}", param);
            TestParamReference(ref param);
            Console.WriteLine("Sortie param={0}", param);

            //int sortie; // 
            TestParamOut(out int sortie);
            Console.WriteLine("Sortie param={0}", sortie);

            TestParamOut(out _);

            TestParamOptionnel(10, "Bonjour", true);
            TestParamOptionnel(10, "Bonjour");
            TestParamOptionnel(10);
            // TestParamOptionnel(12, true); //erreur

            //  Paramètres nommés
            TestParamOptionnel(tst: true, a: 10, str: "Hello");
            TestParamOptionnel(a: 12, tst: true);
            Console.WriteLine(SommeParamVariable("test", 1, 5, 7));
            Console.WriteLine(SommeParamVariable("test", 1, 5, 7, 45, 68));
            Console.WriteLine(SommeParamVariable());

            Console.WriteLine(Somme(1, 2));
            Console.WriteLine(Somme(1, 2, 4));
            Console.WriteLine(Somme(4.0, 2.0));
            Console.WriteLine(Somme(10.0f, 2.0f));

            Console.WriteLine(Factorial(3));

            foreach (var s in args)
            {
                Console.WriteLine(s);
            }
            Console.ReadKey();

            // ExerciceTableau();
        }

        static double Multiplication(double d1, double d2)
        {
            return d1 * d2;
        }

        static void Afficher(string str)
        {
            Console.WriteLine(str);
        }

        static int TestMultiReturn(bool addition, int a, int b)
        {
            if (addition)
            {
                return a + b;
            }
            else
            {
                return a - b;
            }
        }

        #region Exercice_Parite
        static bool Even(int val)
        {
            return val % 2 == 0;
        }

        //static bool Even(int val)
        //{
        //    if (val % 2 == 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //static bool Even(int val)
        //{
        //    return val % 2 == 0 ? true : false;
        //}
        #endregion
        static void TestParamValeur(int x)
        {
            Console.WriteLine("Dans Méthode1 x={0}", x);
            x = 3;
            Console.WriteLine("Dans Méthode2 x={0}", x);
        }
        static void TestParamValeurReference(StringBuilder x)
        {
            Console.WriteLine("Dans Méthode1 x={0}", x);
            //  x = new StringBuilder("Valeur Méthode");
            x.Append(" Ajout Méthode");
            Console.WriteLine("Dans Méthode2 x={0}", x);
        }


        static void TestParamReference(ref int x)
        {
            Console.WriteLine("Dans Méthode1 x={0}", x);
            x = 3;
            Console.WriteLine("Dans Méthode2 x={0}", x);
        }

        static void TestParamOut(out int x)
        {
            //   int y = x * 3;
            x = 3;
            Console.WriteLine("Dans Méthode2 x={0}", x);
        }

        static void TestParamOptionnel(int a, string str = "Default Value", bool tst = false)
        {
            Console.WriteLine("a={0} str={1} tst={2}", a, str, tst);
        }


        static int SommeParamVariable(string nom = "Default", params int[] vals)
        {
            Console.WriteLine(nom);
            int somme = 0;
            foreach (int v in vals)
            {
                somme += v;
            }
            return somme;
        }




        static void AfficherTab(int[] tab)
        {
            Console.Write("[");
            foreach (int t in tab)
            {
                Console.Write(" {0}", t);
            }
            Console.WriteLine(" ]");
        }

        static int[] SaisieTab()
        {
            Console.Write("Entrer la taille du tableau ");
            int size = Convert.ToInt32(Console.ReadLine());

            int[] tabVal = new int[size];
            for (int i = 0; i < tabVal.Length; i++)
            {
                Console.Write("Valeur[{0}]=", i);
                tabVal[i] = Convert.ToInt32(Console.ReadLine());
            }
            return tabVal;
        }

        static void CalculTab(int[] tab, out int minimum, out int maximum, out double moyenne)
        {
            minimum = tab[0];
            maximum = tab[0];
            double somme = tab[0];
            for (int i = 1; i < tab.Length; i++)
            {
                if (tab[i] > maximum)
                {
                    maximum = tab[i];
                }
                if (tab[i] < minimum)
                {
                    minimum = tab[i];
                }
                somme += tab[i];
            }
            moyenne = somme / tab.Length;
        }

        static int ChoixMenu()
        {
            Console.WriteLine("1 - Saisir le tableau");
            Console.WriteLine("2 - Afficher le tableau");
            Console.WriteLine("3 - Afficher le minimum, le maximum et la moyenne");
            Console.WriteLine("0 - Quitter");
            Console.Write("Choix = ");
            return Convert.ToInt32(Console.ReadLine());
        }

        static void ExerciceTableau()
        {
            int[] tab = null;
            int choix;
            do
            {
                choix = ChoixMenu();
                switch (choix)
                {
                    case 1:
                        tab = SaisieTab();
                        if (tab.Length == 0)
                        {
                            tab = null;
                        }
                        break;
                    case 2:
                        if (tab != null)
                        {
                            AfficherTab(tab);
                        }
                        else
                        {
                            Console.WriteLine("Il n'y a pas de tableau saisie");
                        }
                        break;
                    case 3:
                        if (tab != null)
                        {
                            CalculTab(tab, out int minimum, out int maximum, out double moyenne);
                            Console.WriteLine("Minimum={0}  Maximum={1}  Moyenne={2}", minimum, maximum, moyenne);
                        }
                        else
                        {
                            Console.WriteLine("Il n'y a pas de tableau saisie");
                        }
                        break;
                }

            } while (choix != 0);

        }


        // Surcharge Méthode

        static int Somme(int a, int b)
        {
            Console.WriteLine("Entier");
            return a + b;
        }

        static int Somme(int a, int b, int c)
        {
            Console.WriteLine("Entier3");
            return a + b + c;
        }
        static double Somme(double a, double b)
        {
            Console.WriteLine("Double");
            return a + b;
        }

        static float Somme(float f1, float f2)
        {
            Console.WriteLine("Float");
            return f1 + f2;
        }

        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1)// condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }
    }
}
