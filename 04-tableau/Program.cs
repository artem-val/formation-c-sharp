﻿using System;

namespace _04_tableau
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] tab = new double[5];
            tab[0] = 12.8;
            tab[1] = 1.8;
            tab[2] = 2.8;
            tab[3] = 0.8;
            tab[4] = 0.5;
            Console.WriteLine(tab[0]);
            Console.WriteLine(tab.Length);

            char[] tabCh = { 'A', 'Z', 'E', 'R' };

            int taille = Convert.ToInt32(Console.ReadLine());
            string[] tabStr = new string[taille];

            for (int i = 0; i < tabStr.Length; i++)
            {
                tabStr[i] = Console.ReadLine();
            }


            for (int i = 0; i < tabStr.Length; i++)
            {
                Console.WriteLine(tabStr[i]);
            }
            #region Exercice_tableau
            int[] val = { -7, 4, 8, 0, -3 };
            int max = val[0];
            double somme = val[0];
            for (int i = 1; i < val.Length; i++)
            {
                if (val[i] > max)
                {
                    max = val[i];
                }
                somme += val[i];
            }
            Console.WriteLine("maximum={0}, moyenne={1}", max, somme / val.Length);
            #endregion

            #region Exercice_tableau_2
            Console.Write("Entrer la taille du tableau ");
            int size = Convert.ToInt32(Console.ReadLine());

            int[] tabVal = new int[size];
            for (int i = 0; i < tabVal.Length; i++)
            {
                Console.Write("tabVal[{0}]=", i);
                tabVal[i] = Convert.ToInt32(Console.ReadLine());
            }

            int maxTab = tabVal[0];
            double sommeTab = tabVal[0];
            for (int i = 1; i < tabVal.Length; i++)
            {
                if (tabVal[i] > maxTab)
                {
                    maxTab = tabVal[i];
                }
                sommeTab += tabVal[i];
            }
            Console.WriteLine("maximum={0}, moyenne={1}", maxTab, sommeTab / tabVal.Length);
            #endregion
            #region tableau 2 dimmensions
            int[,] tab2d = new int[2, 3];
            Console.WriteLine(tab2d.Length); // le nombre d'élément du tableau 6
            Console.WriteLine(tab2d.Rank); // nombre de dimmension
            Console.WriteLine(tab2d.GetLength(0)); //2
            Console.WriteLine(tab2d.GetLength(1)); //3

            tab2d[0, 0] = 12;
            tab2d[0, 1] = 124;
            Console.WriteLine(tab2d[0, 1]);
            #endregion
            #region tableau n dimmensions
            int[,,,] tabNd = new int[2, 3, 5, 2];
            Console.WriteLine(tabNd.Length); // le nombre d'élément du tableau 
            Console.WriteLine(tabNd.Rank); // nombre de dimmension

            for (int i = 0; i < tabNd.Rank; i++)
            {
                Console.WriteLine(tabNd.GetLength(i));
            }
            tabNd[0, 1, 3, 0] = 1234;
            #endregion
            #region tableau de tableau
            int[][] tabT = new int[4][];
            tabT[0] = new int[3];
            tabT[1] = new int[5];
            tabT[2] = new int[2];
            tabT[3] = new int[4];
            Console.WriteLine(tabT.Length); // nombre de ligne 4
            for (int i = 0; i < tabT.Length; i++)
            {
                Console.WriteLine(tabT[i].Length);
            }
            tabT[0][2] = 45;
            Console.WriteLine(tabT[0][2]);
            //
            int[][] tabTinit = new int[][] { new int[] { 1, 4, 6 }, new int[] { 5, 8, 2, 5, 7 } };
            for (int i = 0; i < tabTinit.Length; i++)
                for (int j = 0; j < tabTinit[i].Length; j++)
                {
                    Console.WriteLine("tab[{0}][{1}]={2}", i, j, tabTinit[i][j]);
                }
            #endregion
            #region foreach
            int[] valeurs = { 12, 6, 8, 3, 9, 3 };
            foreach (int v in valeurs)
            {
                Console.WriteLine(v);
            }
            int[,] valeurs2d = new int[2, 3];
            foreach (int v in valeurs2d)
            {
                Console.WriteLine(v);
            }
            #endregion
            Console.ReadKey();
        }
    }
}
