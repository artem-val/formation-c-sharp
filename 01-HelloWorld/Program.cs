﻿using System;

namespace _01_HelloWorld
{
    // Commentaire en ligne

    /*
     * commentaire
     * sur plusieurs lignes
     */
    /// <summary>
    /// Classe principale
    /// </summary>
    class Program
    {
        /// <summary>
        ///     Méthode principale point d'entrée du programme
        /// </summary>
        /// <param name="args">Les arguments de la ligne de commande</param>
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.ReadKey();
        }
    }
}
