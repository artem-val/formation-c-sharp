﻿using System;

namespace _03_Instructions
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Exercice Salutation
            //Faire un programme qui:
            //- Affiche le message: Entrer votre nom
            //- Permet de saisir le nom
            //- Affiche Bonjour, complété du nom saisie

            Console.Write("Entrer votre nom");
            string msg = Console.ReadLine();
            Console.WriteLine("Bonjour, {0}", msg);
            #endregion

            #region Exercice Sommme
            //Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 + 3 = 4

            int val1 = Convert.ToInt32(Console.ReadLine());
            int val2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("{0}+{1}={2}", val1, val2, val1 + val2);
            #endregion

            #region condition if
            Console.Write("Entrer une valeur ");
            string str = Console.ReadLine();
            int valCondition = Convert.ToInt32(str);
            if (valCondition > 10)
            {
                Console.WriteLine("La valeur saisie est supérieur à 10");
            }

            if (valCondition > 10)
            {
                Console.WriteLine("La valeur saisie est supérieur à 10");
            }
            else
            {
                Console.WriteLine("La valeur saisie est inférieur à 10");
            }

            if (valCondition > 8)
            {
                Console.WriteLine("La valeur saisie est supérieur à 8");
            }
            else if (valCondition > 6)
            {
                Console.WriteLine("La valeur saisie est supérieur à 6");
            }
            else if (valCondition > 2)
            {
                Console.WriteLine("La valeur saisie est supérieur à 2");
            }
            #endregion

            #region Exercice Parité
            // Créer un programme qui indique, si le nombre entier saisie dans la console est paire ou impaire

            Console.Write("Entrer un nombre");
            int val = Convert.ToInt32(Console.ReadLine());
            if (val % 2 == 0)
            {
                Console.WriteLine("Le nombre est paire");
            }
            else
            {
                Console.WriteLine("Le nombre est impaire");
            }
            #endregion

            #region Exercice Intervalle
            //Saisir un nombre et dire s'il fait parti de l'intervalle - 4(exclus) et 7(inclus)

            Console.Write("Entrer un nombre ");
            int vi = Convert.ToInt32(Console.ReadLine());
            if (vi >= -4 && vi < 7)
            {
                Console.WriteLine("Le nombre est dans l'intervalle");
            }
            #endregion

            #region condition switch
            Console.Write("Entrer un jour (entre 1 et 7) ");
            int jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }
            #endregion

            #region Exercice Calculatrice
            //Faire un programme calculatrice
            //Saisir dans la console
            // - un double
            // - une chaine de caractère opérateur qui a pour valeur valide: + - * /
            // - un double
            //Afficher:
            // - Le résultat de l’opération
            // - Une message d’erreur si l’opérateur est incorrecte
            // - Une message d’erreur si l’on fait une division par 0

            Console.Write("Entrer le premier nombre ");
            double d1 = Convert.ToDouble(Console.ReadLine());
            Console.Write("Entrer l'opérateur +, -, *, ou / ");
            string op = Console.ReadLine();
            Console.Write("Entrer le deuxième nombre ");
            double d2 = Convert.ToDouble(Console.ReadLine());
            switch (op)
            {
                case "+":
                    Console.WriteLine("{0} + {1} = {2}", d1, d2, d1 + d2);
                    break;
                case "-":
                    Console.WriteLine("{0} - {1} = {2}", d1, d2, d1 - d2);
                    break;
                case "*":
                    Console.WriteLine("{0} * {1} = {2}", d1, d2, d1 * d2);
                    break;
                case "/":           // En cas de calcul sur un double (valeur approchée), il faut éviter de faire un test d'égalité
                    if (d2 == 0.0) // mais tester un interval (d2>-0.000001 && d2<0.000001) 
                    {
                        Console.WriteLine("Division par 0");
                    }
                    else
                    {
                        Console.WriteLine("{0} / {1} = {2}", d1, d2, d1 / d2);
                    }
                    break;
                default:
                    Console.WriteLine("L'opérateur {0} est incorrecte", op);
                    break;
            }
            #endregion
            // Opérateur ternaire
            int valTer = Convert.ToInt32(Console.ReadLine());
            string strResult = valTer % 2 == 0 ? "Nombre paire" : "Nombre impaire";
            Console.WriteLine(strResult);

            // while =>si la condition est fausse le bloc de code de while n'est pas exécuté
            int k = 0;
            while (k < 5)
            {
                Console.WriteLine("k={0}", k);
                k++;
            }

            k = 0;
            do
            {
                Console.WriteLine("k={0}", k);
                k++;
            } while (k < 5);

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("i={0}", i);
            }

            #region Exercice Table Multiplication
            //Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
            //1 X 4 = 4
            //2 X 4 = 8
            //…
            //9 x 4 = 36
            //Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9, on arrête sinon on redemande une nouvelle valeur
            int mul;
            do
            {
                Console.Write("Saisir un nombre ");
                mul = Convert.ToInt32(Console.ReadLine());
                if (mul > 0 && mul < 10)
                {
                    for (int i = 1; i < 10; i++)
                    {
                        Console.WriteLine("{0} x {1} = {2}", i, mul, i * mul);
                    }
                }
            } while (mul > 0 && mul < 10);
            #endregion
            #region Exercice Quadrillage
            //Quadrillage
            //Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
            //ex: pour 2 3

            //[ ][ ]
            //[ ][ ]
            //[ ][ ]
            Console.Write("Saisir le nombre de colonne ");
            int col = Convert.ToInt32(Console.ReadLine());
            Console.Write("Saisir le nombre de ligne ");
            int ligne = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < ligne; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    Console.Write("[ ] ");
                }
                Console.WriteLine("");
            }
            #endregion

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("i={0}", i);
                if (i == 3)
                {
                    break;
                }
            }
            for (int j = 0; j < 3; j++)
            {
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine("i={0} j={1}", i, j);
                    if (i == 3)
                    {
                        break;
                    }
                }
            }


            int multi;
            do
            {
                Console.Write("Saisir un nombre ");
                multi = Convert.ToInt32(Console.ReadLine());
                if (multi <= 0 || multi >= 10)
                {
                    break;
                }
                for (int i = 1; i < 10; i++)
                {
                    Console.WriteLine("{0} x {1} = {2}", i, multi, i * multi);
                }
            } while (true);
          //  for (; ; ) // Boucle infinie avec un for
          //  {

          //  }

            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    continue;
                }
                Console.WriteLine("i={0}", i);
            }

            for (int j = 0; j < 3; j++)
            {
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine("i={0} j={1}", i, j);
                    if (i == 3)
                    {
                        goto EXIT;
                    }
                }
            }

        EXIT: Console.Write("Entrer un jour (entre 1 et 7) ");
             jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    goto case 7;
               //  break;
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            Console.ReadKey();
        }
    }
}
