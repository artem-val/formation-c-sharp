# Compléments

## Caractères Spéciaux

| Caractères Spéciaux | Description |
| ------ | ------ |
| __\n__ | Nouvelle ligne |
| __\r__ | Retour chariot |
| __\f__ | Saut de page   |
| __\t__ | Tabulation horizontale |
| __\v__ | Tabulation verticale |
| __\0__ | Caractère nul |
| __\a__ | Alerte |
| __\b__ | Retour arrière |
| __\\\\__ | Backslash |
| __\\'__ | Apostrophe |
| __\\"__ | Guillemet |

## Raccourcis clavier Visual Studio

|  |  | |
| ------ | ------ |------ |
| __Ctrl k__ | __Ctrl d__ | Indentation du document |
| __Ctrl k__ | __Ctrl f__ | Indentation de la sélection |
| __Ctrl k__ | __Ctrl :__ | Commenté / Décommenté la sélection |
| __Ctrl__ | __espace__ | Auto-Complétion (Intellisence)|

