﻿using System;

namespace _02_SyntaxeBase
{
    // Structure
   struct Point
    {
        public int X;
        public int Y;
    }
    class Program
    {
        // Une enumération est un ensemble de constante
        enum Motorisation { ESSENCE = 23, DIESEL = 34, ELECTRIQUE = 92, HYDROGENE = 1 }

        // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
        enum Direction : short { NORD = 90, SUD = 270, EST = 0, OUEST = 180 }

        // Énumération comme indicateurs binaires
        // Pour éviter toute ambiguïté, il faut assigner explicitement tous les membres
        [Flags]
        enum JourSemaine
        {
            LUNDI = 1,
            MARDI = 2,
            MERCREDI = 4,
            JEUDI = 8,
            VENDREDI = 16,
            SAMEDI = 32,
            DIMANCHE = 64,
            WENKEND = SAMEDI | DIMANCHE  // On peut spécifier des  combinaisons de membres pendant la déclaration
        }
        static void Main(string[] args)
        {

            #region Type de Donnée
            // Déclaration d'une variable   type nomVariable;
            int i;
            //Console.WriteLine(i); // Erreur => on ne peut pas utiliser une variable non initialisée

            // Initialisation    nomVariable = valeur
            i = 42;
            Console.WriteLine(i);

            // Déclaration multiple de variable 
            double hauteur, largeur;
            hauteur = 10.0;
            largeur = 45.0;
            Console.WriteLine(hauteur + " " + largeur);  // Concaténation avec +

            // Déclaration et initialisation de variable    type nomVariable=valeur;
            int j = 12;
            Console.WriteLine(j);

            // Littéral caractère
            char ch = 'a';
            char chUtf = '\u0045';
            char chHexaUTF = '\x45';
            Console.WriteLine(ch + " " + chUtf + " " + chHexaUTF);

            // Littéral booléen
            bool tst = false;
            Console.WriteLine(tst);

            // Littéral entier ->int par défaut
            long l = 123L;  // Litéral long
            uint ui = 123U; // Litéral unsigned
            Console.WriteLine(l + " " + ui);

            // Littéral nombre à virgule flottante -> double par défaut
            float f = 123.0f;       // Litéral float
            decimal dec = 123.0M;   // Litéral decimal
            Console.WriteLine(f + " " + dec);

            // Littéral entier -> chagement de base
            int deci = 42; // décimal (base 10) par défaut
            int hexa = 0xF4; // 0x -> héxadecimal
            int bin = 0b10011010; // 0b -> binaire

            Console.WriteLine(deci + " " + hexa + " " + bin);

            // Séparateur _
            int sep = 1_000_000;
            // double sd = _100_._00_;  // pas de _ en début, en fin , avant et après la virgule 
            Console.WriteLine(sep);

            // Type implicite -> var
            var v = 12.5; // v double
            Console.WriteLine(v);
            #endregion

            #region Conversion
            // Transtypage implicite ( pas de perte de donnée)
            // type inférieur => type supérieur
            short s = 123;
            double d1 = s;
            Console.WriteLine(s + " " + d1);
            int it = 23;
            long lt = it;

            // Perte de precision long -> float
            long lp1 = 123456789;
            long lp2 = 123456788;
            float fp1 = lp1;
            float fp2 = lp2;
            long diffl = lp1 - lp2;
            float difff = fp1 - fp2;
            Console.WriteLine(diffl + " " + difff); // 1    8

            // Transtypage explicite: cast = (nouveauType)
            int ite = 12;
            short ste = (short)ite;
            double dte = 45.9;
            ite = (int)dte;
            Console.WriteLine(ste + " " + ite + " " + dte);

            ite = 245;
            sbyte sbe = (sbyte)ite;      // Dépassement de capacité
            Console.WriteLine(ite + " " + sbe);

            // Checked / Unchecked
            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral

            int che = int.MaxValue;     // int.MaxValue => valeur maximal que peut contenir un entier
            //checked
            //{
            //    Console.WriteLine(che + 1);   //  avec checked une exception est générée
            //}

            unchecked
            {
                Console.WriteLine(che + 1);      // Dépassement de capacité
            }

            // Fonction de Convertion
            // La classe Convert contient des méthodes statiques permettant de convertir  un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérque
            double dconv = 234.90;
            int iConv = Convert.ToInt32(dconv);
            Console.WriteLine(iConv);

            string strConv = "1234";
            iConv = Convert.ToInt32(strConv);
            Console.WriteLine(iConv);

            // Les classes Int32, Double... ont une méthode Parse qui permet la convertion d'une chaine de caractère vers le type  
            iConv = Int32.Parse(strConv);
            #endregion

            // Type référence
            string str1 = "hello";
            string str2 = null;
            Console.WriteLine(str1 + " " + str2);
            str2 = str1;
            Console.WriteLine(str1 + " " + str2);

            // Nullable : permet d'utiliser une valeur null avec un type valeur
            int? itt = null;
            Console.WriteLine(itt.HasValue);
            itt = 1234;
            Console.WriteLine(itt.HasValue);
            Console.WriteLine(itt.Value);
            Console.WriteLine((int)itt);

            // Constante
            const int cstI = 123;
            //cstI = 45;     // Erreur: on ne peut pas modifier la valeur d'une constante
            //const double azerty;
            const string cstStr = "Bonjour";
            Console.WriteLine(cstI + " " + cstStr);

            // Enumération
            // motor est une variable qui ne pourra accepter que les valeurs de l'enum Motorisation
            Motorisation motor = Motorisation.ELECTRIQUE;
            // La méthode toString convertit une constante enummérée en une chaine de caractère
            Console.WriteLine(motor.ToString());  // string  Affiche Electrique
            Console.WriteLine((int)motor);  // int  On peut convertir un énumération en entier 

            motor = (Motorisation)92;       // int-> Motorisation
            // La méthode Parse permet de convertir une chaine de caractère ou la valeur de l'enumération en une constante enummérée
            motor = (Motorisation)Enum.Parse(typeof(Motorisation), "DIESEL"); // String -> Motoristion
            Console.WriteLine(motor.ToString());  // string
            Console.WriteLine((int)motor);  // int

            // Énumération comme indicateurs binaires: utilisation
            JourSemaine jour = JourSemaine.LUNDI | JourSemaine.MARDI;
            Console.WriteLine((jour & JourSemaine.WENKEND) != 0);  // teste la présence de SAMEDI ou DIMANCHE => false
            jour = JourSemaine.SAMEDI;
            Console.WriteLine((jour & JourSemaine.WENKEND) != 0);  //  teste la présence de SAMEDI ou DIMANCHE => true

            // Structure
            Point pt1;
            pt1.X = 2;
            pt1.Y = 4;
            Point pt2 = pt1;
            Console.WriteLine(pt2.X + " " + pt2.Y);

            // Littérale chaine de caractère -> " "
            string str = "hello";
            Console.WriteLine(str);

            // Format pour les chaines de caractères
            Console.WriteLine(string.Format("x={0}, y={1}", pt2.X, pt2.Y));
            Console.WriteLine("x={0}    y={1}", pt2.X, pt2.Y); // on peut définir directement le format dans la mèthode WriteLine 
            Console.WriteLine($"x={pt2.X}    y={pt2.Y}");


            #region operateur
            // Opérateur arithmétique
            int a = 12 + 30;
            int b = a * 3;
            int m = a % 2;
            Console.WriteLine("a={0} b={1} m={2}", a, b, m);

            // Opérateur d'incrémentation (idem pour la décrémentation)
            // Pre-Incrementation
            int inc = 0;
            int res = ++inc;
            Console.WriteLine("inc={0} res={1}", inc, res); //1 1

            //Post-incrementation
            inc = 0;
            res = inc++;
            Console.WriteLine("inc={0} res={1}", inc, res); //1 0

            // Affectation
            inc = 23;
            // Affectation combinée
            inc += 10; // inc=inc+10  //33
            inc /= 3; // inc=inc/3   //11
            Console.WriteLine(inc);

            // Opérateur de comparaison
            bool tst1 = inc == 11;
            Console.WriteLine("tst1={0} tst2={1}", tst1, inc > 100);

            // Opérateur court circuit
            bool tst3 = inc == 11 && inc < 100; // true
            bool tst4 = inc != 11 && inc < 100; // false

            bool tst5 = inc == 11 || inc < 100; // true

            string str22 = "azerty";// = null;
            bool tst6 = str22 != null && str22.Length == 6;
            Console.WriteLine(tst6);

            byte bin1 = 0b01111;
            Console.WriteLine(~bin1); // 11110000 Opérateur ~ => complémént: 1 -> 0 et 0 -> 1
            Console.WriteLine(bin1 & 0b00001);      // et bit à bit
            Console.WriteLine(bin1 | 0b10000);      // ou           => 11111
            Console.WriteLine(bin1 ^ 0b01010);      // ou exclusif  =>  101 5
            Console.WriteLine(0b101 >> 1);          // Décalage à droite de 1 10 2
            Console.WriteLine(0b101 << 1);          // Décalage à gauche de 1 1010 
            bin1 &= 0b10110; // bin1=bin1&0b10110   -> 110 6
            Console.WriteLine(bin1);

            // L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null ; sinon, il évalue l’opérande de droite et retourne son résultat.
            string str4 = null;
            string z1 = str4 ?? "val defaut"; // Opérateur ?? :si str4 est null z a pour valeur "aval defaut" sinon str4
            str4 = "azerty";
            string z2 = str4 ?? "val defaut";
            Console.WriteLine("z1={0} z2={1}", z1, z2);
            #endregion
            Console.ReadKey();
        }
    }
}
